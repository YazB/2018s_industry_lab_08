package ictgradschool.industry.io.ex01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        String message = "";
        try (FileReader fR = new FileReader("input2.txt")) {
            int read;
            while (( read = fR.read()) != -1) {
                total++;

                if(read=='e'||read=='E') {
                    numE++;
                }
            }
        } catch (IOException e) {
            System.out.println("File not found problem " + e);
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        try (BufferedReader reader = new BufferedReader(new FileReader("input2.txt"))) {
            String line = null;

            while (( line=reader.readLine())!=null) {
                total+=line.length();

                for (int i = 0; i < line.length(); i++) {
                    int read=line.charAt(i);
                    if(read=='e'||read=='E') {
                        numE++;
                    }
                }

            }
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
