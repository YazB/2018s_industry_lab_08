package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {
        int FileReader;

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
        try (BufferedReader reader = new BufferedReader(new FileReader("input1.txt"))) {
            String line;
            while ((line=reader.readLine())!=null) {
                System.out.println(line);
                String text = Keyboard.readInput();

                if (text.isEmpty()) {
                    break;
                }
                System.out.println(text + "\n");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}

