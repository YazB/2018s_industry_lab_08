package ictgradschool.industry.io.ex02;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {
        File myFile = new File("input1.txt");
        try (Scanner scanner = new Scanner(myFile)) {
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            // TODO Prompt the user for a file name, then read and print out all the text in that file.
            // TODO Use a Scanner.
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
    }
    public static void main(String[] args) {
        new MyScanner().start();
    }
}
