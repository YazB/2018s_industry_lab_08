package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        File myFile = new File("myformattedfile.txt");
        try(Scanner scanner = new Scanner (fileName)) {
            if(fileName==null) {
                throw new IOException("Operation has failed");
            }
            while(scanner.hasNextLine()) {
                    System.out.println(scanner.nextLine());
            }
        } catch(IOException e) {
            System.out.println("Error: "+e.getMessage());
        }

        // TODO Implement this with a Scanner

        return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
