package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter
        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
        // Use a for loop to go through each film in film array
            for (int i = 0; i <films.length ; i++) {
               Movie film=films[i];
                writer.print(film.getName() +",");
                writer.print(film.getYear()+ ",");
                writer.print(film.getLengthInMinutes()+ ",");
                writer.print(film.getDirector());
                writer.print("\n");
            }
            //

            //writer.println()
        } catch (IOException e) {
            System.out.println("Error:" + e);
        }
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
